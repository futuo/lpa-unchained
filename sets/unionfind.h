typedef struct _set{
    int p;
    int rank;
}set;

int set_find(set *forest, int x);
void set_union(set *forest, int x, int y);
