#include "unionfind.h"

void set_union(set *forest, int x, int y){
    
    int xRoot = find(forest, x),
        yRoot = find(forest, y);

    if( forest[xRoot].rank < forest[yRoot].rank )
        forest[yRoot].p = xRoot;

    else if( forest[yRoot].rank < forest[xRoot].rank )
        forest[xRoot].p = yRoot;

    else{
        forest[yRoot].p = xRoot;
        forest[xRoot].rank++;
    }

}

int set_find(set *forest, int x){

    if( forest[x].p == x )
        return x;

    int p = find( forest[x].p );
    forest[x].p = p;
    return p;

}
